#!/usr/bin/env python3

import paho.mqtt.client as mqtt
import queue
import json
import time
import traceback

import numpy as np
from point_pillars import PointPillarsNet

sub_topics = [ #(topic, qos)
    ("start/ZCU104", 2),
    ("data/ZCU104",  2),
]

def on_connect(client, userdata, flags, rc):
    pass

def on_disconnect(client, userdata, rc):
    print("Disconnected with result code {}".format(rc))

def on_message(client, userdata, msg):
    try:
        userdata.put(msg)
    except Exception:
        pass

def subscribe_to_topics(client):
    for topic, qos in sub_topics:
        client.subscribe(topic, qos)

def send_start_indicator(client):
    topic   = "start/PC"
    payload = "Start!"
    qos     = 2
    retain  = False
    client.publish(topic, payload, qos, retain)

def wait_for_PC(msgQueue):
    TIMEOUT = 15 #sec
    time_start = time.time()
    ret = False
    while time.time() - time_start < TIMEOUT:
        try:
            msg = msgQueue.get(timeout=0.5)
            if msg.topic == "start/ZCU104":
                print("Connected with PC")
                ret = True
                break
        except queue.Empty:
            pass
    return ret

def wait_for_data(msgQueue):
    def _unpack_data(data):
        return int(data.decode("ascii"))
    TIMEOUT = 60 #sec
    time_start = time.time()
    ret = None
    while time.time() - time_start < TIMEOUT:
        try:
            msg = msgQueue.get(timeout=0.5)
            if msg.topic == "data/ZCU104":
                ret = _unpack_data(msg.payload)
                break
        except queue.Empty:
            pass
    return ret

def compute_response(pc_num):
    try:
        ret = pp_net.forward(pc_num)
    except Exception as e:
        print("compute_response exception: {}\n{}".format(e, traceback.format_exc()))
        ret = None
    return ret

def send_data(client, data):
    topic   = "data/PC"
    payload = data
    qos     = 2
    retain  = False
    client.publish(topic, payload, qos, retain)

class TimeoutError(Exception):
    pass

# Message queue
msgQueue = queue.Queue()

# PointPillarsNetwork
pp_net = PointPillarsNet()

# MQTT broker/connection specific data
BROKER = "192.168.2.37"
CLIENT_ID = "TheZCU104"
PORT = 1883

# Setup the client
mqttClient = mqtt.Client(client_id = CLIENT_ID, userdata = msgQueue)
mqttClient.on_connect = on_connect
mqttClient.on_disconnect = on_disconnect
mqttClient.on_message = on_message

try:
    # Connect to broker
    mqttClient.connect(host=BROKER, port=PORT, keepalive=15)
    mqttClient.loop_start()

    # Subscribe to topics
    subscribe_to_topics(mqttClient)

    # Send start indicator to PC
    if not wait_for_PC(msgQueue):
        raise TimeoutError

    send_start_indicator(mqttClient)

    end_flag = False
    while not end_flag:
        in_data = wait_for_data(msgQueue)
        if in_data is not None:
            print("\nNew point cloud came!")
            time_start = time.time()
            out_data = compute_response(in_data)
            time_end = time.time()
            send_data(mqttClient, out_data.tobytes())
        else:
            break

     #send_stop() #TODO

except Exception as e:
    print("Exception: {}".format(e))
finally:
    mqttClient.loop_stop()
