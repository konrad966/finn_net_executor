#!/usr/bin/env python3

on_ZCU104 = True

import numpy as np
from random import seed
import random
import time
if on_ZCU104:
    from driver import FINNAccelDriver

def multithreshold(v, thresholds, out_scale=None, out_bias=None):
    is_global_threshold = thresholds.shape[0] == 1
    assert (
        v.shape[1] == thresholds.shape[0]
    ) or is_global_threshold, """"Threshold
    shape incorrect"""
    # save the required shape sizes for the loops (N, C and B)
    num_batch = v.shape[0]
    num_channel = v.shape[1]
    num_act = thresholds.shape[1]
    
    d = (thresholds[0,-1] - thresholds[0,0])/thresholds.shape[1]

    v = v // d
    v = np.clip(v, 0, thresholds.shape[1])

    if out_scale is None:
        out_scale = 1.0
    if out_bias is None:
        out_bias = 0.0
    return out_scale * v + out_bias

class FC:
    def __init__(self, in_features, out_features, weights):
        """ Fully connected layer.
        :param in_features: <int> Number of input channels.
        :param out_features: <int> Number of output channels.
        :param weights: <np.array> of size (in_features, out_features)
        """

        assert weights.shape[0] == in_features and weights.shape[1] == out_features, "Weights not aligned to features"
        self.weights = weights

    def forward(self, x):
        x = np.matmul(x, self.weights)
        return x

class BN_ReLU:
    def __init__(self, gamma, beta, bn_mean, bn_var):
        """ Batch normalization and ReLU."""
        self.eps=1e-3
        self.gamma = gamma
        self.beta = beta
        self.mean = bn_mean
        self.var = bn_var

    def batch_norm(self, x):
        x = (x - self.mean)/np.sqrt(self.var + self.eps) * self.gamma + self.beta
        return x

    def relu(self, x):
        return np.maximum(0, x)


class PFN:
    def __init__(self, in_features, out_features, weights, gamma, beta, bn_mean, bn_var):
        """ Pillar Feature Net Layer.
        :param in_features: <int> Number of input channels.
        :param out_features: <int> Number of output channels.
        :param weights: <np.array> of size (in_features, out_features)
        """

        self.fc = FC(in_features, out_features, weights)
        self.bn_relu = BN_ReLU(gamma, beta, bn_mean, bn_var)

    def forward(self, x):
        x = self.fc.forward(x)
        x = self.bn_relu.batch_norm(x)
        x = self.bn_relu.relu(x)

        # to get features for whole voxel
        x = x.max(axis=0)
        return x


def extend_point_features(voxel, voxel_size, cloud_range, num_voxel_x, num_voxel_y, num_points):
    """ This function extend 4 point coordinates to 9 features. """

    # Find distance of x, y, and z from cluster center
    points_mean = np.sum(voxel[:, :3], axis=0) / num_points
    f_cluster = voxel[:, :3] - points_mean

    # Find distance of x, y, and z from pillar center
    x_c = (num_voxel_x * voxel_size[0] + cloud_range[0]) + 0.5 * voxel_size[0]
    y_c = (num_voxel_y * voxel_size[1] + cloud_range[1]) + 0.5 * voxel_size[1]

    f_center = np.zeros((voxel.shape[0], 2))
    f_center[:, 0] = voxel[:, 0] - x_c
    f_center[:, 1] = voxel[:, 1] - y_c

    # Combine together feature decorations
    features_extended = np.concatenate((voxel, f_cluster, f_center), axis=1)
    features_extended[num_points:, :] = 0

    return features_extended


def PFN_scatter(features_voxel, num_x, num_y, voxel_size, cloud_range):
    # Initialized array of shape according to point cloud range
    cols_x = np.int((cloud_range[3] - cloud_range[0]) // voxel_size[0])
    rows_y = np.int((cloud_range[4] - cloud_range[1]) // voxel_size[1])
    canvas = np.zeros((1, 64, rows_y, cols_x)) #NCHW

    for i, f_v in enumerate(features_voxel):
        canvas[0, :, num_y[i], num_x[i]] = f_v
    return canvas


def load_pc(filename):
    lidar_pc = np.fromfile(filename, dtype=np.float32).reshape(-1, 4)
    return lidar_pc

def voxelize_pc(point_cloud, voxel_size, point_cloud_range, points_in_voxel, voxels_num):
    tolerance = 0.01
    voxels = {}
    # limit point cloud to point_cloud_range
    condition = np.logical_and(point_cloud_range[0] <= point_cloud[:,0], point_cloud[:,0] < point_cloud_range[3] - tolerance)
    condition = np.logical_and(condition, point_cloud_range[1] <= point_cloud[:,1])
    condition = np.logical_and(condition, point_cloud[:,1] < point_cloud_range[4] - tolerance)
    condition = np.logical_and(condition, point_cloud_range[2] <= point_cloud[:,2])
    condition = np.logical_and(condition, point_cloud[:,2] < point_cloud_range[5] - tolerance)
    point_cloud = point_cloud[condition,:]
    pc_shape = point_cloud.shape

    # shuffle the point cloud in order to randomly choose P pillars
    idx = np.arange(point_cloud.shape[0])
    idx = np.random.shuffle(idx)
    point_cloud = point_cloud[idx, :]
    point_cloud = point_cloud.reshape(pc_shape) #for some reason the point_cloud arr becomes 3D after the above line

    # pillar coordinates for each point
    num_x = ((point_cloud[:,0] - point_cloud_range[0]) // voxel_size[0]).astype(np.int32)
    num_y = ((point_cloud[:,1] - point_cloud_range[1]) // voxel_size[1]).astype(np.int32)

    num_x_list = num_x.tolist()
    num_y_list = num_y.tolist()
    num_x_num_y = list(zip(num_x_list, num_y_list))
    num_x_num_y = list(set(num_x_num_y))
    voxels = []
    voxels_coords = []
    voxels_numpoints = []
    # The loop below takes the most time, maybe there's some way to reduce it to numpy ops?
    for curr_x, curr_y in num_x_num_y:
        cond = np.logical_and(
            num_x == curr_x,
            num_y == curr_y
        )
        voxel = point_cloud[cond,:]
        voxels_numpoints.append(voxel.shape[0])
        if voxel.shape[0] > points_in_voxel:
            voxel = voxel[:points_in_voxel, :]
            voxels_numpoints[-1] = points_in_voxel
        elif voxel.shape[0] < points_in_voxel:
            tmp = np.zeros((points_in_voxel - voxel.shape[0], 4), dtype=voxel.dtype)
            voxel = np.concatenate([voxel, tmp], axis=0)
        voxels.append(voxel)
        voxels_coords.append((curr_x, curr_y))
        if len(voxels) == voxels_num:
            break
    return voxels, voxels_coords, voxels_numpoints

class BackboneSSD:
    def __init__(self, bitfile, threshold_file, add_file, mul_file):
        batch_size = 1
        self.finnDriver = FINNAccelDriver(batch_size, bitfile)

        self.mt_context = {
            "thresholds" : np.load(threshold_file),
            "out_scale"  : 1.0, #check the value in the FINN
            "out_bias"   : 0.0, #check the value in the FINN
        }

        self.add_context = {
            "tensor" : np.load(add_file),
        }

        self.mul_context = {
            "tensor" : np.load(mul_file),
        }

    def _preprocess(self, x):
        x = multithreshold(
            x,
            self.mt_context.get("thresholds"),
            self.mt_context.get("out_scale"),
            self.mt_context.get("out_bias"),
        )
        x = np.transpose(x, (0, 2, 3, 1)) #"NCHW --> NHWC"
        return x
    
    def _postprocess(self, x):
        x = np.transpose(x, (0, 3, 1, 2)) #"NHWC --> NCHW"
        x = x * self.mul_context.get("tensor")
        x = x + self.add_context.get("tensor")
        return x

    def forward(self, x):
        time_preprocessing_start = time.time()
        x = self._preprocess(x)
        x = self.finnDriver.fold_input(x)
        x = self.finnDriver.pack_input(x)
        self.finnDriver.copy_input_data_to_device(x)
        time_preprocessing_end = time.time()
        print("FPGA preprocessing (on ARM) took {:.2f} seconds".format(time_preprocessing_end - time_preprocessing_start))

        time_execute_start = time.time() 
        self.finnDriver.execute()
        time_execute_end = time.time()
        print("FPGA part took {:.2f} seconds".format(time_execute_end - time_execute_start))

        time_postprocessing_start = time.time()
        x = self.finnDriver.unpack_output(self.finnDriver.obuf_packed_device)
        x = self.finnDriver.unfold_output(x)
        x = self._postprocess(x)
        time_postprocessing_end = time.time()
        print("FPGA postprocessing (on ARM) took {:.2f} seconds".format(time_postprocessing_end - time_postprocessing_start))
        return x


class PointPillarsNet:
    def __init__(self,
        pfn_in_features  = 9,
        pfn_out_features = 64,
        points_in_voxel  = 100,
        voxels_num       = 12000,
        pc_range         = [0, -25.6, -3, 51.201, 25.601, 1],
        voxel_size       = [0.16, 0.16],
        pc_name_format   = "input_data/{:06d}.bin",
        gamma_filename   = "net_params/gamma.npy",
        beta_filename    = "net_params/beta.npy",
        mean_filename    = "net_params/mean.npy",
        var_filename     = "net_params/var.npy",
        weights_filename = "net_params/weights.npy",
        bitfile          = "net_params/resizer.bit",
        threshold_file   = "net_params/thresholds.npy",
        add_file         = "net_params/add_params.npy",
        mul_file         = "net_params/mul_params.npy",
    ):
        # Load network parameters
        weights = np.load(weights_filename)
        gamma   = np.load(gamma_filename)
        beta    = np.load(beta_filename)
        bn_mean = np.load(mean_filename)
        bn_var  = np.load(var_filename)
        assert weights.shape == (pfn_in_features, pfn_out_features)
        # assert gamma.shape  == TODO
        # assert beta.shape  == TODO
        
        # Initialize modules
        self.pfn = PFN(
            pfn_in_features,
            pfn_out_features,
            weights,
            gamma,
            beta,
            bn_mean,
            bn_var,
        )
        if on_ZCU104:
            self.backbonessd = BackboneSSD(
                bitfile,
                threshold_file,
                add_file,
                mul_file
            )

        # Save additional data
        self.points_in_voxel = points_in_voxel
        self.voxels_num = voxels_num
        self.pc_range = pc_range
        self.voxel_size = voxel_size
        self.pc_name_format = pc_name_format

    def forward(self, pc_num):
        time_inference_start = time.time()
        pc = load_pc(self.pc_name_format.format(pc_num))

        time_voxelization_start = time.time()
        voxels, voxels_coords, voxels_numpoints = voxelize_pc(
            pc,
            self.voxel_size,
            self.pc_range,
            self.points_in_voxel,
            self.voxels_num
        )
        time_voxelization_end = time.time()
        print("Voxelization took {:.2f} seconds".format(time_voxelization_end - time_voxelization_start))

        time_extend_start = time.time()
        features_cloud = []
        for i, v in enumerate(voxels):
            num_x, num_y = voxels_coords[i]
            features = extend_point_features(
                v, 
                self.voxel_size, 
                self.pc_range, 
                num_x, 
                num_y,
                voxels_numpoints[i],
            )
            voxel_feature = self.pfn.forward(features)
            features_cloud.append(voxel_feature)
        tmp = list(zip(*voxels_coords))
        num_x = tmp[0]
        num_y = tmp[1]
        features_cloud = np.stack(features_cloud, axis=0)
        time_extend_end = time.time()
        print("Extending feature vector took {:.2f} seconds".format(time_extend_end - time_extend_start))

        time_scatter_start = time.time()
        x = PFN_scatter(
            features_cloud, 
            num_x, 
            num_y, 
            self.voxel_size, 
            self.pc_range
        )
        time_scatter_end = time.time()
        print("Scatter operation took {:.2f} seconds".format(time_scatter_end - time_scatter_start))

        if on_ZCU104:
            x = self.backbonessd.forward(x)

        time_inference_end = time.time()
        print("Total inference time: {:.2f} seconds".format(time_inference_end - time_inference_start))
        return x


def test():
    filename = "000000.bin"
    seed(int(time.time()))
    pc = load_pc(filename)
    features_cloud = []
    num_x_cloud = []
    num_y_cloud = []
    weight = np.ones((9, 64))
    v_size = (0.16, 0.16)
    #point_cloud_range = (0, -34.56, -3, 69.12, 34.56, 1)
    point_cloud_range = (0, -34.72, -3, 69.12, 34.721, 1)
    gamma = 1
    beta = 1
    N = 100
    P = 120000
    
    voxels, voxels_coords = voxelize_pc(pc, v_size, point_cloud_range, N, P)

    #voxels[341].astype(np.float32).tofile("out.bin")
    pfn = PFN(9, 64, weight, gamma, beta)

    for i, v in enumerate(voxels):
        num_x, num_y = voxels_coords[i]
        features = extend_point_features(v, v_size, point_cloud_range, num_x, num_y)
        voxel_feature = pfn.forward(features)
        features_cloud.append(voxel_feature)
        num_x_cloud.append(num_x)
        num_y_cloud.append(num_y)
        break
    tmp = list(zip(*voxels_coords))
    num_x = tmp[0]
    num_y = tmp[1]
    features_cloud = np.stack(features_cloud, axis=0)
    PFN_scatter(features_cloud, num_x, num_y, v_size, point_cloud_range)
    

if __name__ == "__main__":
    if True: #check extend_point_features
        in_tensor = np.load("voxels_PC9.npy")
        voxels = in_tensor[:,:,:4]
        features_cloud = []
        for i, v in enumerate(voxels):
            num_points = 0
            for point in v:
                if point[0] == 0 and point[1] == 0 and point[2] == 0 and point[3] == 0:
                    pass
                else:
                    num_points += 1
            out, _, _ = extend_point_features(
                v,
                [0.16, 0.16],
                [0, -25.6, -3, 51.201, 25.601, 1],
                None,
                None,
                num_points,
            )
            features_cloud.append(out)
        out_tensor = np.stack(features_cloud, axis=0)
        print(in_tensor)
        print(out_tensor)
        diff = np.abs(in_tensor[:,:,7:] - out_tensor[:,:,7:])
        diff = diff.reshape(1,-1)
        print("max: {}".format(np.max(diff)))
        print("mean: {}".format(np.mean(diff)))
        print("var: {}".format(np.var(diff)))
    
    if False: #check scatter
        scatter_in  = np.load("scatter_in.npy")
        scatter_out = np.load("scatter_out.npy")
        points      = np.load("voxels_PC9.npy")
        print(points.shape)
        voxels = points[:,:,:4]
        nums_x = []
        nums_y = []
        for i, v in enumerate(voxels):
            num_points = 1
            _, num_x, num_y = extend_point_features(
                v,
                [0.16, 0.16],
                [0, -25.6, -3, 51.201, 25.601, 1],
                None,
                None,
                num_points,
            )
            nums_x.append(num_x)
            nums_y.append(num_y)
        # features_cloud = np.stack(features_cloud, axis=0)
        out = PFN_scatter(
            scatter_in, 
            nums_x, 
            nums_y, 
            [0.16, 0.16],
            [0, -25.6, -3, 51.201, 25.601, 1],
        )
        print(out.shape)
        print(scatter_out.shape)
        diff = np.abs(scatter_out - out)
        diff = diff.reshape(1,-1)
        print("max: {}".format(np.max(diff)))
        print("mean: {}".format(np.mean(diff)))
        print("var: {}".format(np.var(diff)))


    