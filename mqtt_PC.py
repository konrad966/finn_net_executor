#!/usr/bin/env python3

import paho.mqtt.client as mqtt
import queue
import json
import time
import numpy as np
import torch
from second.pytorch.train import PPLoaderPredicter
from visualization import plot

sub_topics = [ #(topic, qos)
    ("start/PC", 2),
    ("data/PC",  2),
]

def on_connect(client, userdata, flags, rc):
    pass

def on_disconnect(client, userdata, rc):
    print("Disconnected with result code {}".format(rc))

def on_message(client, userdata, msg):
    try:
        userdata.put(msg)
    except Exception:
        pass

def subscribe_to_topics(client):
    for topic, qos in sub_topics:
        client.subscribe(topic, qos)

def send_start_indicator(client):
    topic   = "start/ZCU104"
    payload = "Start!"
    qos     = 2
    retain  = False #True
    client.publish(topic, payload, qos, retain)

def wait_for_ZCU104(msgQueue):
    TIMEOUT = 15 #sec
    time_start = time.time()
    ret = False
    while time.time() - time_start < TIMEOUT:
        try:
            msg = msgQueue.get(timeout=0.5)
            if msg.topic == "start/PC":
                print("Connected with ZCU104")
                ret = True
                break
        except queue.Empty:
            pass
    return ret

def send_data(client, data):
    topic   = "data/ZCU104"
    payload = data
    qos     = 2
    retain  = False
    client.publish(topic, payload, qos, retain)

def wait_for_data(msgQueue):
    def _unpack_data(data):
        shape = (1, 20, 160, 160)
        ret = np.frombuffer(data, dtype=np.float32)
        ret = ret.reshape(shape)
        return ret
    TIMEOUT = 90 #sec
    time_start = time.time()
    ret = None
    while time.time() - time_start < TIMEOUT:
        try:
            msg = msgQueue.get(timeout=0.5)
            if msg.topic == "data/PC":
                ret = _unpack_data(msg.payload)
                break
        except queue.Empty:
            pass
    return ret

class TimeoutError(Exception):
    pass

# PointPillars example loader (and predicter!)
pplp = PPLoaderPredicter(
    config_path = "/home/konradl/magisterka/repo/second.pytorch/second/configs/pointpillars/car/xyres_16_finn.proto"
)


# Message queue
msgQueue = queue.Queue()

# MQTT broker/connection specific data
BROKER = "192.168.2.37"
CLIENT_ID = "ThePC"
PORT = 1883

# Setup the client
mqttClient = mqtt.Client(client_id = CLIENT_ID, userdata = msgQueue)
mqttClient.on_connect = on_connect
mqttClient.on_disconnect = on_disconnect
mqttClient.on_message = on_message

pcrange = [0, -25.6, -3, 51.201, 25.601, 1]

try:
    # Connect to broker
    mqttClient.connect(host=BROKER, port=PORT, keepalive=15)
    mqttClient.loop_start()

    # Subscribe to topics
    subscribe_to_topics(mqttClient)

    # Send start indicator to ZCU104
    send_start_indicator(mqttClient)
    if not wait_for_ZCU104(msgQueue):
        raise TimeoutError

    # Send pointclouds
    """
    You can specify more pointclouds in pc_nums, but you have to copy 
    appropiate *.bin point clouds and *.png images from KITTI dataset.
    What is more, they have to be from evaluation dataset.
    """
    pc_nums = [8, 19, 21]
    for pc_num in pc_nums:
        example = pplp.load_new_example()
        in_data = int(example.get("image_idx")[0])
        while in_data != pc_num:
            example = pplp.load_new_example()
            in_data = int(example.get("image_idx")[0])
        send_data(mqttClient, in_data)
        out_data = wait_for_data(msgQueue)
        if out_data is not None:
            print("\nInput: {}".format(in_data))
            torch_tensor = torch.from_numpy(out_data)
            print("torch shape: {}".format(torch_tensor.shape))
            preds = pplp.predict(example, torch_tensor) #with batch == 1, preds have only one element!
            if len(preds) > 0:
                preds = preds[0]
                boxes_bev = preds.get("box3d_lidar").detach().numpy()
                boxes_img = preds.get("bbox").detach().numpy()
                scores    = preds.get("scores").detach().numpy()
                pc_filename = "input_data/{:06d}.bin".format(in_data)
                img_filename = "input_data/{:06d}.png".format(in_data)
                plot(
                    pc_filename,
                    img_filename,
                    scores,
                    boxes_bev,
                    boxes_img,
                    pcrange,
                )
        else:
            print("ZCU104 not responding")
            break
    
    time.sleep(5)
    #send_stop() #TODO

except Exception as e:
    import traceback
    print("Exception: {}\n{}".format(e, traceback.format_exc()))
finally:
    mqttClient.loop_stop()
