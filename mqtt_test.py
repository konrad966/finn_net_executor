#!/usr/bin/env python3

import paho.mqtt.client as mqtt
import queue
import json
import time

def on_connect(client, userdata, flags, rc):
    pass

def on_disconnect(client, userdata, rc):
    print("Disconnected with result code {}".format(rc))

def on_message(client, userdata, msg):
    try:
        print("Message: topic: {}, payload: {}".format(msg.topic, msg.payload))
    except Exception:
        pass


# MQTT broker/connection specific data
BROKER = "192.168.2.37"
CLIENT_ID = "TheHost"
PORT = 1883

# Setup the client
msgQueue = queue.Queue()
mqttClient = mqtt.Client(client_id = CLIENT_ID, userdata = msgQueue)
mqttClient.on_connect = on_connect
mqttClient.on_disconnect = on_disconnect
mqttClient.on_message = on_message

# Connect to broker
mqttClient.connect(host=BROKER, port=PORT, keepalive=15)
mqttClient.loop_start()

# Subscribe to data topic
topic = "test_topic"
qos   = 2
mqttClient.subscribe(topic, qos=qos)

# Publish some message
payload = "TestTestTest"
mqttClient.publish(topic, payload, qos)

# Loop
try:
    time.sleep(3)
except Exception as e:
    print("Exception: {}".format(e))
finally:
    mqttClient.loop_stop()
