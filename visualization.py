
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.transforms as transforms
import time
import numpy as np


def load_pc(filename):
    lidar_pc = np.fromfile(filename, dtype=np.float32).reshape(-1, 4)
    return lidar_pc


def plot(filename_pc, filename_image, scores, boxes_bev, boxes_img, pcrange):
    # np.set_printoptions(formatter={'float': lambda x: "{0:0.3f}".format(x)})
    pc = load_pc(filename_pc)
    pc = pc[pc[:,2]>pcrange[2],:]
    pc = pc[pc[:,2]<pcrange[5],:]

    thresh = 0.5

    image = plt.imread(filename_image)

    fig, (ax1, ax2) = plt.subplots(2)
    ax2.set_ylim([pcrange[0], pcrange[3]])
    ax2.set_xlim([pcrange[4], pcrange[1]])
    ax2.set_aspect("equal")
    ax1.imshow(image)
    figManager = plt.get_current_fig_manager()
    figManager.window.showMaximized()
    plt.scatter(pc[:, 1], pc[:, 0], s=0.5, c=pc[:, 2])
    for i in range(0, boxes_bev.shape[0]):
        if scores[i] < thresh:
            continue
        xc = boxes_bev[i, 0]
        yc = boxes_bev[i, 1]
        w  = boxes_bev[i, 3]
        l  = boxes_bev[i, 4]
        theta = boxes_bev[i, 6]
        # Create a Rectangle patch
        rect = patches.Rectangle((yc - 0.5*l, xc - 0.5*w), l, w, linewidth=2, edgecolor='r', facecolor='none')
        t = transforms.Affine2D().rotate_around(yc, xc, theta) + ax2.transData
        rect.set_transform(t)
        # Add the patch to the Axes
        ax2.add_patch(rect)

        x1 = boxes_img[i, 0]
        y1 = boxes_img[i, 1]
        x2 = boxes_img[i, 2]
        y2 = boxes_img[i, 3]
        rect = patches.Rectangle((x1, y1), x2-x1, y2-y1, linewidth=2, edgecolor='r', facecolor='none')
        ax1.add_patch(rect)
    plt.draw()
    plt.pause(1)

if __name__ == "__main__":
    #test3
    filename_img = "000001.png"
    filename_bev = "000001.bin"
    boxes_img = [[20, 40, 21, 21]]
    boxes_bev = [[0, 0, 0, 10, 5, 3, np.pi/4]]
    scores = [0.4]
    plot(filename_bev, filename_img, scores, boxes_bev, boxes_img, [-10, -25.6, -3, 80.201, 25.601, 1])
    time.sleep(10)
