## Description

This is a *finn_net_executor* repository containing ZCU104 implementation of PointPillars network.
It contains PC host code (tested on Ubuntu 18.04 LTS) and ZCU104 code (tested on PYNQ v2.5).


## Getting started

To run code in the repository you have to:

1. Clone the repo ```git clone https://gitlab.com/konrad966/finn_net_executor.git```

2. Clone PointPillars forked repository ```git clone -b mag_pointpillars_quantized_demo https://gitlab.com/konrad966/pointpillars_quantized.git```

3. Run through README of cloned PointPillars repository - install all necessary dependencies and make sure it is included in the PYTHONPATH environment variable.

4. Download the <a href="https://mosquitto.org/download/" target="_blank">Mosquitto</a> MQTT broker.

5. Run the broker with default settings (port 1883, no TLS, no authentication) by typing: ```sudo systemctl start mosquitto.service```

6. Change the BROKER ip address in **mqtt_PC.py** and **mqtt_ZCU104.py**. Use the one that your PC use to communicate with the ZCU104.

7. Copy the *finn_net_executor* repository content to a ZCU104 board with the PYNQ envirnoment.

8. Clone repository ```git clone https://github.com/eclipse/paho.mqtt.python.git``` and copy its **src** dir content to ZCU104, to the same directory in which *finn_net_executor* repository content is.

9. Open terminal on the PC, go to *finn_net_executor* repo directory and activate conda environment you use for the PointPillars network. Install **paho.mqtt** package by typing ```pip install paho.mqtt```. Prepare a command ```python mqtt_PC.py```, but do not execute it yet. You will do it in the 11th point, and you will have 15 seconds to do it after executing point number 10.

10. Power up the board, log into it via SSH (or in any other way, terminal is that what matters), go into directory with copied repo content and run command ```sudo python3 mqtt_ZCU104.py```. 

11. Execute command ```python mqtt_PC.py``` on the PC.

12. Sit and watch car detections coming to the PC minute by minute.

## Running custom network

To generate a custom network it is convenient to use <a href="https://gitlab.com/konrad966/finn_fork" target="_blank">this</a> FINN forked repository.
It generates proper bitstream and network driver that you need to copy to the board.
You may need to modify code in *finn_net_executor* repository a little, especially if input/output tensor shapes or data types change.